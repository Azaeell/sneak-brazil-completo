const openButton = document.querySelector('.open');
const closeButton = document.querySelector('.close');
const body = document.querySelector('body');
const modal = document.querySelector('.layout');

const handleToggleModal = () => {
    modal.show();
};
const close = () => {
    modal.close();
};
openButton.addEventListener('click', handleToggleModal);
closeButton.addEventListener('click', close);